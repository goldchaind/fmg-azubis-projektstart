package fmg.ausbildungsstart.projekt.logic;


import static org.junit.Assert.fail;
import org.junit.Test;

public class TestBackgroundProcess {



    @Test
    public void testRun(){
        AirplaneManager manager=new AirplaneManager();
        BackgroundProcess process=new BackgroundProcess(manager);

        Thread backgroundThread=new Thread(process);
        backgroundThread.start();

        wait10ms();
        if(process.isRunning()==false){
            fail("Backgroundprocess läuft nicht");
        }



    }

    @Test
    public void testStop(){

    }

    @Test
    public void testIsRunning(){
        AirplaneManager manager=new AirplaneManager();
        BackgroundProcess process=new BackgroundProcess(manager);

        Thread backgroundThread=new Thread(process);
        backgroundThread.start();

        wait10ms();
        if(process.isRunning()==true){
            process.stop();
            //wait();
        }

    }

    private void wait10ms(){
        try{
            Thread.sleep(10);
        }catch(InterruptedException e){

        }
    }


}

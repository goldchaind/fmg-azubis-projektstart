package fmg.ausbildungsstart.projekt.logic;



import java.util.Date;
import static org.junit.Assert.fail;
import org.junit.Test;

public class TestAirplane {



    @Test
    public void allesImKonstruktor() {
        Date myDate = new Date();
        Airplane airplane = new Airplane(1, false, "TestFlug123", new Date(myDate.getTime()), "FlugzeugTyp", "MUC", "JFK");
        if (airplane.getRunnway() != 1) {
            fail();
        }
        if (airplane.isLanding()) {
            fail();
        }
        if (!airplane.getFlightNumber().equals("TestFlug123")) {
            fail();
        }
        if (airplane.getStartTime().getTime() != myDate.getTime()) {
            fail();
        }
        if (!airplane.getAirplaneType().equals("FlugzeugTyp")) {
            fail();
        }
        if (!airplane.getFlightFrom().equals("MUC")) {
            fail();
        }
        if (!airplane.getFlightTo().equals("JFK")) {
            fail();
        }
    }

    @Test
    public void allesEinzeln() {
        Date myDate = new Date();
        Airplane airplane = new Airplane();
        airplane.setRunnway(1);
        if (airplane.getRunnway() != 1) {
            fail();
        }
        //airplane.setLanding(false);
        if (airplane.isLanding()) {
            fail();
        }
        airplane.setFlightNumber("TestFlug123");
        if (!airplane.getFlightNumber().equals("TestFlug123")) {
            fail();
        }
        airplane.setStartTime(new Date(myDate.getTime()));
        if (airplane.getStartTime().getTime() != myDate.getTime()) {
            fail();
        }
        airplane.setAirplaneType("FlugzeugTyp");
        if (!airplane.getAirplaneType().equals("FlugzeugTyp")) {
            fail();
        }
        airplane.setFlightFrom("MUC");
        if (!airplane.getFlightFrom().equals("MUC")) {
            fail();
        }
        airplane.setFlightTo("JFK");
        if (!airplane.getFlightTo().equals("JFK")) {
            fail();
        }
    }

    @Test
    public void testgetXPosition() {
        Date myDate = new Date();
        Airplane starting = new Airplane(1, false, "TestFlug123", new Date(myDate.getTime()), "FlugzeugTyp", "MUC", "JFK");
        Airplane landing = new Airplane(1, true, "TestFlug123", new Date(myDate.getTime()), "FlugzeugTyp", "MUC", "JFK");

        double a = 2000 / Math.pow(AirplaneManager.timeToTakeoff / 1000, 2);

        for (int movdir = 0; movdir <= 1; movdir++) {
            starting.setMovingDirection(movdir);
            for (int time = 0; time < AirplaneManager.timeToTakeoff / 1000; time += 1) {
                int x = starting.getXPosition(myDate.getTime() + time * 1000);
                double soll = 0;
                if (starting.getMovingDirection() == 1) {
                    soll = (a / 2 * Math.pow(time, 2));
                } else {
                    soll = 1000 - (int) (a / 2 * Math.pow(time, 2));
                }
                test(x == (int) soll, "Falsche Start Formel für MovingDirection: " + starting.getMovingDirection());
            }
            landing.setMovingDirection(movdir);
            for (int time = 0; time < AirplaneManager.timeToTakeoff / 1000; time += 1) {
                int x = landing.getXPosition(myDate.getTime() + time * 1000);
                double landingtime = (int) (AirplaneManager.timeToTakeoff - time * 1000) / 1000;
                double soll;//
                if (landing.getMovingDirection() == 1) {
                    soll = (1000 - (int) (a / 2 * Math.pow(landingtime, 2)));
                } else {
                    soll = (int) (a / 2 * Math.pow(landingtime, 2));
                }
                test(x == soll, "Falsche Lande Formel für MovingDirection: "+landing.getMovingDirection());
            }

        }

        test(landing.getXPosition(myDate.getTime() + (long) AirplaneManager.timeToTakeoff + 1) == -1000, "Flugzeug wird beim landen nach Stillstand nicht auf -1000 positioniert");

    }

    public void test(boolean totest, String msg) {
        if (!totest) {
            fail(msg);
        }
    }
}

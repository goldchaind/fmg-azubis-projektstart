/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fmg.ausbildungsstart.projekt.logic;

import java.util.Date;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gjurajd
 */
public class TestAirplaneManager {

    AirplaneManager manager;
    public TestAirplaneManager() {
        manager=new AirplaneManager();
    }
    
    
    
    @Test
    public void TestisRequiredToLoadNextHour(){
        //AirplaneManager manager=new AirplaneManager();       
       
        manager.airplanesNextHour=new ArrayList<>();
        test(manager.isRequiredToLoadNextHour()==false,"Falschen Wert für isRequiredToLoadNextHour zurück gegeben");
        manager.airplanesNextHour=null;
        test(manager.isRequiredToLoadNextHour(),"Falschen Wert für isRequiredToLoadNextHour zurück gegeben");
        
    }
    
    @Test
    public void TestgetCurrentTime(){
        //AirplaneManager manager=new AirplaneManager();   
        manager.currentTime=new Date(System.currentTimeMillis());
        test(manager.currentTime==manager.getCurrentTime(),"Falschen Wert für CurrentTime zurück gegeben");       
    }
    
    @Test
    public void TestgetTimeSpeed(){
        test(manager.timeSpeed==manager.getTimeSpeed(),"Falschen Wert für TimeSpeed zurück gegeben");         
    }
    
    @Test
    public void TestgetFormattedTime(){        
        test(manager.guiOutputTime.format(manager.currentTime).equals(manager.getFormattedTime()),"Falscher Wert für FormattedTime");        
    }
    
    @Test
    public void TestchangeAirplanesOnHourChange(){
        AirplaneManager tmpManager=new AirplaneManager();
        
        tmpManager.airplanesThisHour=null;
        tmpManager.airplanesNextHour=null;
        
        tmpManager.changeAirplanesOnHourChange();
        test(tmpManager.airplanesNextHour==null && tmpManager.airplanesThisHour==null  , "Wenn airplanesNextHour null ist, darf nichts verändert werden");
        
        ArrayList<Airplane> airplanes=new ArrayList<>();
        tmpManager.airplanesNextHour=airplanes;
        tmpManager.changeAirplanesOnHourChange();
        test(tmpManager.airplanesNextHour==null && tmpManager.airplanesThisHour==airplanes,"Wenn airplanesNextHour nicht null ist, müssen die Werte verändert werden");

    }
    
    /*@Test
    public void TestpauseApplication(){       
      BackgroundProcess process=manager.backgroundProcess;
      manager.pauseApplication();
      wait(100);
      test(process.isRunning==false,"BackgroundProcess wurde nicht gestoppt");
      test(manager.backgroundProcess==null,"BackgroundProcess wurde nicht auf null gesetzt");
      test(manager.lastRefresh==-1,"lastRefresh nicht zurückgesetzt");
    }*/
    
    @Test
    public void TestrefreshTime(){        
        AirplaneManager tmpManager=new AirplaneManager();
        tmpManager.pauseApplication();
        tmpManager.currentTime=null;
        tmpManager.refreshTime();        
        test(tmpManager.lastRefresh==System.currentTimeMillis() && tmpManager.currentTime.equals(new GregorianCalendar(2019,8,10,6,50,0).getTime()),"Fehler im Fall currentTime==null");
        
        tmpManager.lastRefresh=-1;
        tmpManager.refreshTime();
        test(tmpManager.lastRefresh==System.currentTimeMillis(),"Fehler im Fall lastRefresh<0");
        
        tmpManager.refreshTime();
        Date date=new Date();
        date.setTime((long) (tmpManager.currentTime.getTime() + (System.currentTimeMillis() - tmpManager.lastRefresh) * tmpManager.timeSpeed));
        test(tmpManager.lastRefresh==System.currentTimeMillis() && tmpManager.currentTime.equals(date),"Fehler im fall lastRefresh >0");
    }
    
    /*@Test
    public void TestloadAirplanesThisHour(){
         test(manager.airplanesThisHour.size()==manager.database.getFlightsByHour(manager.currentTime).size(),"Daten wurden nicht richtig geladen");
    }*/
    
    @Test
    public void TestloadAirplanesNextHour(){
        manager.loadAirplanesNextHour();
        test(manager.airplanesNextHour.size()==manager.database.getFlightsNextHour(manager.currentTime).size(),"Daten wurden nicht richtig geladen");
    }

    
    
    private void wait(int ms){
        try{
            Thread.sleep(ms);
        }catch(InterruptedException e){

        }
    }
    
        public  void test(boolean totest,String msg){
        if(!totest){
            fail(msg);
        }
    }
   
}

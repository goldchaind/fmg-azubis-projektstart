/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fmg.ausbildungsstart.projekt.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gjurajd
 */
public class TestGruppe3 {
    
    @Test
    public void testRun(){
        AirplaneManager manager=new AirplaneManager();
        manager.timeSpeed=100;
        
        if(manager.airplanesThisHour.size()!=36 || manager.airplanesNextHour !=null){
            fail("Fehler, Airplanemanager funktionen werden nicht korrekt aufgerufen!");
        }
         wait(500);        
       
        if(manager.airplanesThisHour.size()!=10 || manager.airplanesNextHour.size() !=54){
            fail("Fehler, Airplanemanager funktionen werden nicht korrekt aufgerufen!");
        }        
      
        wait(1000);       
         if(manager.airplanesThisHour.size()!=8 || manager.airplanesNextHour.size() !=54){
            fail("Fehler, Airplanemanager funktionen werden nicht korrekt aufgerufen!");
        }
        
    }

    @Test
    public void testStop(){
        AirplaneManager manager=new AirplaneManager();
        BackgroundProcess process=new BackgroundProcess(manager);

        Thread backgroundThread=new Thread(process);
        backgroundThread.start();

         wait(100);
         if(process.isRunning==false){
            fail("Backgroundprocess wurde nicht gestartet, kann also nicht beendet werden");
        }
        process.stop();
        wait(100);
        if(process.isRunning==true){
            fail("Backgroundprocess läuft trotz Aufruf von stop");
        }
    }

    @Test
    public void testIsRunning(){
        AirplaneManager manager=new AirplaneManager();
        BackgroundProcess process=new BackgroundProcess(manager);
        process.isRunning=false;
        if(process.isRunning()!=false){
            fail("isRunning gibt den falschen Wert zurück");
        }
        process.isRunning=true;
        if(process.isRunning()!=true){
            fail("isRunning gibt den falschen Wert zurück");
        }

    }

    private void wait(int ms){
        try{
            Thread.sleep(ms);
        }catch(InterruptedException e){

        }
    }

}

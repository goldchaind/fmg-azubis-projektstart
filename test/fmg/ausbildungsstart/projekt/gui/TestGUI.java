package fmg.ausbildungsstart.projekt.gui;
import fmg.ausbildungsstart.projektDone.logic.Airplane;
import java.util.Date;
import static org.junit.Assert.fail;
import org.junit.Test;

public class TestGUI {

    @Test
    public void testGUIKonstruktor(){
        GUI gui=new GUI(null);
        test(gui.getX()==200,"Fenster soll auf X-Position 200 sein");
        test(gui.getY()==0,"Fenster soll auf Y-Position 0 sein");
        test(gui.getWidth()==1100,"Fenster soll 1100 pixel breit sein");
        test(gui.getHeight()==700,"Fenster soll 700 pixel hoch sein");
        test(gui.getTitle()=="Azubi Einführungsprojekt","Titel soll \"Azubi Einführungsprojekt\" sein" );
        test(gui.isVisible(),"Fenster muss sichtbar sein");
    }

    @Test
    public void testAddAirplane(){
        GUI gui=new GUI(null);
        Airplane a=new Airplane();
        gui.addAirplane(a);
        test(gui.airplanes.contains(a),"Flugzeug wurde nicht zur Liste hinzugefügt");
    }


    public  void test(boolean totest,String msg){
        if(!totest){
            fail(msg);
        }
    }

}

package fmg.ausbildungsstart.projekt.logic;

import java.util.Calendar;

public class BackgroundProcess implements Runnable{

    private AirplaneManager airplaneManager;
    boolean isRunning = false;
    private boolean stopRequest = false;
    private int timeToSleep = 10000;


    /**
     *  Konstruktor der einen AirplaneManager erwartet.
     *  Der AirplaneManager muss in eine globale Variable gespeichert werden.
     *  Die Zeit die zwischen zwei schleifendurchläufen gewartet werden soll muss berechnet werden.
     *  Basierend auf der im Airplane Manager eingestellten Geschwindigkeit soll die Wartezeit berechnet
     *  werden. Dabei gilt: je höher die Geschwindigkeit, desto geringer soll die Wartezeit werden.
     *  (Mehr FPS). In unserem Fall nehmen wir eine Wartezeit von 60ms als Ausgangszeit. Von dieser
     *  wird alle 5 Geschwindigkeitsschritte (Geschwindigkeit aus AirplaneManager) 1ms abgezogen.
     *  Überprüft zusätzlich, bis zu welcher Geschwindigkeit diese Berechnung sinnvolle Ergebnisse
     *  liefert und unterbindet fehlerhafte Werte.
     *
     * @param airplaneManager
     */
    public BackgroundProcess(AirplaneManager airplaneManager) {
        this.airplaneManager = airplaneManager;
        this.timeToSleep = 60 - (int) ((airplaneManager.getTimeSpeed()/5)) ; //  + 200;
        if(timeToSleep < 1){
            throw new RuntimeException("Zu schnell!");
        }
        System.out.println("TimeToSleep = " + timeToSleep);
    }

    /**
     * Die Methode run() soll keine Logik implementiert bekommen, sie soll lediglich die
     * Methoden des AirplaneManagers aufrufen.
     */
    @Override
    public void run() {
        isRunning = true;
        stopRequest = false;

        Calendar calendar = Calendar.getInstance();

        // Hauptschleife, die die Verarbeitung steuert
        // Soll laufen, solange die Anwendung läuft und kein stopRequest existiert
        while(isRunning && !stopRequest){

            long loopStartTime = System.currentTimeMillis();


            // Zeit aktualisieren
            airplaneManager.refreshTime();

            // Abfragen der aktuellen Minute
            calendar.setTime(airplaneManager.getCurrentTime());

            // Wenn Minute 50 vorbei ist sollen die Flüge der nächsten Stunde geladen werden
            if(calendar.get(Calendar.MINUTE) > 50 && airplaneManager.isRequiredToLoadNextHour()){
                airplaneManager.loadAirplanesNextHour();
            }

            // Bei Minute 0 sollen die Flugzeuge von der nächsten Stunde auf die aktuelle Stunde umgespeichert werden.
            if(calendar.get(Calendar.MINUTE) == 0){
                airplaneManager.changeAirplanesOnHourChange();
            }

            // Flugzeuge mit einer Startzeit kleiner als aktuelle Zeit sollen zur GUI hinzugefügt werden
            airplaneManager.addNextAirplanesToGUI();

            airplaneManager.updateGUI();

            //Warten berechnet sich nach der Geschwindigkeit
            try {
                // Berechnung der Schlafzeit. Die Zeit die vergangen ist, seit dem die Variable
                // loopStartTime gesetzt wurde soll von der Schlafzeit abgezogen werden. Wenn die
                // berechnete Schlafzeit kleiner als 0 ist soll nicht geschlafen werden.
                long sleepTime = loopStartTime + timeToSleep - System.currentTimeMillis();
                if(sleepTime >0) {
                    Thread.sleep(sleepTime);
                }else{
                    System.out.println("KLEINER ALS 0!!");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //Beendet
        isRunning = false;
    }

    /**
     * Soll das stopRequest-Flag setzen, damit der Prozess beendet wird.
     */
    public void stop() {
        stopRequest = true;
    }

    /**
     * Soll zurückliefern, ob der Prozess gerade läuft oder nicht.
     * @return
     */
    public boolean isRunning() {
        return isRunning;
    }
}
